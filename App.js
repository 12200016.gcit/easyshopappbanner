import { StatusBar } from "expo-status-bar";
import { StyleSheet, Text, View } from "react-native";
import ProductCard from "./screens/Products/ProductCard";
import ProductContainer from "./screens/Products/ProductContainer";
import ProductList from "./screens/Products/ProductList";
import Header from "./Shared/Header";
import ProductDetails from "./screens/Products/ProductDetails";
import { SafeAreaView } from "react-native-safe-area-context";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import Home from "./screens/Products/Home";
import Ionicons from "@expo/vector-icons/Ionicons";
import Settings from "./screens/Products/Settings";
import SearchBox from "./components/Products/SearchBox";
const Stack = createNativeStackNavigator();

export default function App() {
  const Tab = createBottomTabNavigator();

  function StackNavigaton() {
    return (
      <Stack.Navigator>
        <Stack.Screen
          name="productContainer"
          component={ProductContainer}
          options={{ title: "Product Container" }}
        ></Stack.Screen>
        <Stack.Screen
          name="ProductDetails"
          component={ProductDetails}
          options={{ title: "Product Details" }}
        ></Stack.Screen>
      </Stack.Navigator>
    );
  }
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar style="auto" />
      <Header />
      <NavigationContainer>
        <Tab.Navigator
          screenOptions={{
            tabBarActiveTintColor: "blue",
            tabBarInactiveInt: "green",
            tabBarActiveBackgroundColor: "Black",
            // tabBarShowLabel: false,
          }}
        >
          <Tab.Screen
            name="Home"
            component={Home}
            options={{
              tabBarIcon: ({ size, color }) => (
                <Ionicons name="home" color={color} size={size} />
              ),
            }}
          ></Tab.Screen>
          <Tab.Screen
            name="Settings"
            component={Settings}
            options={{
              tabBarIcon: ({ size, color }) => (
                <Ionicons name="settings" color={color} size={size} />
              ),
            }}
          ></Tab.Screen>

          <Tab.Screen
            name="ProductContainer"
            component={StackNavigaton}
            options={{
              tabBarIcon: ({ size, color }) => (
                <Ionicons name="list" color={color} size={size} />
              ),
              headerShown: false,
            }}
          ></Tab.Screen>
        </Tab.Navigator>
      </NavigationContainer>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: "white",
    // alignItems: "center",
    // justifyContent: "center",
    // marginTop: 80,
  },
});
