import { Image, StyleSheet, View, Text, Button } from "react-native";
function Header() {
  return (
    <View style={styles.imageContainer}>
      <Image
        style={styles.image}
        resizeMode="contain"
        source={require("../assets/logo.jpg")}
      />
      {/* <Text>hi</Text> */}
    </View>
    // <Button title="Search" color="Blue" />
  );
}

export default Header;
const styles = StyleSheet.create({
  image: {
    width: "100%",
    height: "100%",
  },
  imageContainer: {
    height: 50,
    width: "100%",
    alignItems: "center",
    marginTop: 10,
  },
});
