import { View, Text, StyleSheet, Image, Dimensions } from "react-native";
import Swiper from "react-native-swiper";
import React, { useEffect, useState } from "react";

const height = Dimensions.get("window").height;
export default function Banner() {
  const [banner, setBanner] = useState([]);
  useEffect(() => {
    setBanner([
      "https://images.vexels.com/media/users/3/126443/preview2/ff9af1e1edfa2c4a46c43b0c2040ce52-macbook-pro-touch-bar-banner.jpg",
      "https://pbs.twimg.com/media/D7P_yLdX4AAvJWO.jpg",
      "https://www.yardproduct.com/blog/wp-content/uploads/2016/01/gardening-banner.jpg",
    ]);
  }, []);
  return (
    <View style={styles.root}>
      <Swiper autoplay={true} autoplayTimeout={3} showsButtons={true}>
        {banner.map((image) => (
          <View key={image} style={styles.imageContainer}>
            <Image style={styles.image} key={image} source={{ uri: image }} />
          </View>
        ))}
      </Swiper>
    </View>
  );
}

const styles = StyleSheet.create({
  root: {
    // backgroundColor: "red",
    width: "100%",
    height: height / 4,
    overflow: "hidden",
  },
  image: {
    width: "100%",
    height: "100%",
    borderRadius: 10,
  },
  imageContainer: {
    paddingHorizontal: 35,
  },
});
