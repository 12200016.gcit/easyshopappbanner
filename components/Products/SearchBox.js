import { View, Text, TextInput, StyleSheet } from "react-native";
import React from "react";
import { Ionicons } from "@expo/vector-icons";

export default function SearchBox({ onChangeText }) {
  return (
    <View style={styles.searchContainer}>
      <Ionicons style={styles.icon} name="search" size={27}></Ionicons>
      <TextInput
        style={styles.input}
        placeholderTextColor="black"
        placeholder="search"
        onChangeText={onChangeText}
      ></TextInput>
    </View>
  );
}

const styles = StyleSheet.create({
  searchContainer: {
    backgroundColor: "blue",
    // display: "flex",
    flexDirection: "row",
    // color: 'white'
    padding: 10,
  },
  icon: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",

    color: "black",
    backgroundColor: "white",
    padding: 7,
  },
  input: {
    paddingStart: 14,
    flex: 10,
    color: "black",
    backgroundColor: "white",
    // padding: 5,
  },
});
