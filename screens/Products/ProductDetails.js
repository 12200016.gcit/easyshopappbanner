import React from "react";
import { View, Text, Image, StyleSheet } from "react-native";

const ProductDetails = ({ route }) => {
  return (
    <View style={styles.container}>
      <View>
        <Image
          source={{
            uri: route.params.image
              ? route.params.image
              : "https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg",
          }}
          style={styles.image}
        />
        <Text style={styles.name}>{route.params.name}</Text>
        <Text style={styles.price}>{route.params.price}</Text>
        <Text style={styles.description}>{route.params.description}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
    backgroundColor: "white",
  },
  image: {
    // backgroundColor: "red",
    width: "100%",
    height: 200,
    resizeMode: "cover",
  },
  detailsContainer: {
    marginTop: 16,
  },
  name: {
    fontSize: 24,
    fontWeight: "bold",
  },
  price: {
    fontSize: 18,
    color: "gray",
    marginTop: 8,
  },
  description: {
    fontSize: 16,
    marginTop: 16,
  },
});

export default ProductDetails;
