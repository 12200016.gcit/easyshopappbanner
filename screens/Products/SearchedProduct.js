import { View, Text, FlatList, StyleSheet } from "react-native";
import React from "react";
import SearchedItem from "../../components/Products/SearchedItem";

export default function SearchedProduct({ searchedProduct }) {
  return (
    <View style={styles.container}>
      <FlatList
        data={searchedProduct}
        renderItem={({ item }) => <SearchedItem product={item}></SearchedItem>}
        keyExtractor={(item) => item._id.$oid}
      ></FlatList>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
