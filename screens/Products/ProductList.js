import { View, Text, Dimensions, StyleSheet } from "react-native";

import React from "react";
import { TouchableOpacity } from "react-native";
import ProductCard from "../Products/ProductCard";
const { width } = Dimensions.get("window");
import { useNavigation } from "@react-navigation/native";
console.log(width);
function ProductList({ product }) {
  const navigation = useNavigation();
  return (
    <TouchableOpacity
      style={styles.rootContainer}
      onPress={() => navigation.navigate("ProductDetails", product)}
    >
      <View style={styles.body}>
        <ProductCard product={product} />
      </View>
    </TouchableOpacity>
  );
}
export default ProductList;

const styles = StyleSheet.create({
  rootContainer: {
    width: "50%",
  },
  body: {
    width: width / 2,
    backgroundColor: "gray",
    alignItems: "center",
    justifyContent: "center",
    textAlignVertical: "center",
  },
});
